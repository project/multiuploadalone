<?php
/**
 * @file
 * Internal functions for Multiple Upload Alone module.
 */

/**
 * Get a file from an upload_element and save contained files in {upload} table
 *
 * A blank line forms a paragraph. There should be no trailing white-space
 * anywhere.
 *
 * @param $node
 *   Node object where the files will be attached.
 *
 * @param $element
 *   Name of the archive form element (upload_element).
 *
 * @param $mode
 *   Unzip mode supported.
 *   @todo transform to drupal variable if php will be supported.
 *
 * @return
 *   TRUE on error (now used only for bogus mode).
 */
function _multiuploadalone_upload($node, $element = 'upload_archive', $mode = 'exec') {
  $file = $node->{$element};
  if (isset($file->filepath)) {
    $dest_dir = $file->filepath .'_multiuploadalone_'. time() .'/';
    // create a writable directory
    mkdir($dest_dir, 0777);
    // @todo should I replace above with:
    //    require_once realpath('includes/install.inc');
    //    drupal_install_mkdir($dest_dir, 0744);
    switch ($mode) {
      // Unzip command required:
      case 'exec':
        // Unzip to $dest_dir, ignoring directory structure (-j):
        // If files are contained in one or more directories, no subdirectory will be created.
        $cmd = 'unzip -qqj '. escapeshellarg($file->filepath) .' -d '. escapeshellarg($dest_dir);
        exec($cmd, $output, $return_vars);
      break;
      /**
       * Bogus mode.
       * @todo Add PHP support?  How to preserve all files within directory
       * structure like unzip -j?
       */
      case 'php':
        return FALSE;
      break;
    }

    /** Save each file on directory and save it to db moving to files directory **/
    _multiuploadalone_file2db(scandir($dest_dir), $dest_dir, $node);
    /** Delete temporary directory and files, eventually generating an error message **/
    $status = _multiuploadalone_delete($file, $dest_dir);
    if ($status['errors']) {
      foreach ($status['fail_on_upload'] as $failfilename) {
        drupal_set_message(t('Cannot upload %file due to permission or security reasons. File skipped on multiple upload.', array('%file' => $failfilename)));
      }
    }
  }
  // success
  return TRUE;
}

/**
 * Check for valid entries among uncompressed files and save to database.
 *
 * @param $filenames_raw
 *   Array of file names to create.
 *
 * @param $tmp_dir
 *   Temporary source directory of decompressed files.
 */
function _multiuploadalone_file2db($filenames_raw, $tmp_dir, $node) {
  $filenames = array();
  foreach ($filenames_raw as $filename) {
    // Ignore backup, hidden, backup and system files
    if (drupal_strlen(trim($filename, ".")) > 0) {
      // Make temporary file writable
      chmod($tmp_dir .'/'. $filename, 0777);
      $filenames[] = $filename;
    }
  }
  if (!empty($filenames)) {
    _multiuploadalone_writefile($filenames, $tmp_dir, file_directory_path(), $node);
  }
}

/**
 * Save valid file(s) record on database.
 *
 * Move files from temporary directory to installation file directory.
 *
 * A blank line forms a paragraph. There should be no trailing white-space
 * anywhere.
 *
 * @param $filenames
 *   Array of file names to create.
 *
 * @param $src_dir
 *   Temporary source directory of decompressed files.
 *
 * @param $dest
 *   Destination directory.
 */
function _multiuploadalone_writefile($filenames, $src_dir, $dest, $node) {
  global $user;
  // @see http://api.drupal.org/api/function/upload_node_form_submit/6
  $limits = _upload_file_limits($user);
  $validators = array(
    'file_validate_extensions' => array($limits['extensions']),
    'file_validate_image_resolution' => array($limits['resolution']),
    'file_validate_size' => array($limits['file_size'], $limits['user_size']),
  );

  // Destination transliteration
  module_load_include('inc', 'transliteration', 'transliteration');

  // Default list setting from File uploads configuration page
  $list = variable_get('upload_list_default', 1);

  /**
   * @todo How to preserve original file name into file description?
   * Now the transliterated filename is used as description.
   */
  foreach ($filenames as $filename) {
    $filepath_tmp  = $src_dir .'/'. $filename;
    $filepath_dest = $dest .'/'. transliteration_clean_filename($filename);

    // Adapted from file_save_upload()
    // @see http://api.drupal.org/api/drupal/includes--file.inc/6/source
    // Build the list of non-munged extensions.
    // @todo this should not be here. we need to figure out the right place.
    $extensions = '';
    foreach ($user->roles as $rid => $name) {
      $extensions .= ' '. variable_get("upload_extensions_$rid",
      variable_get('upload_extensions_default', 'jpg jpeg gif png txt html doc xls pdf ppt pps odt ods odp'));
    }

    // Begin building file object.
    $file = new stdClass();
    $file->filename = file_munge_filename(trim(basename($filepath_dest), '.'), $extensions);
    $file->filepath = $filepath_dest;
    $file->filemime = file_get_mimetype($file->filename);

    // If the destination is not provided, or is not writable, then use the
    // temporary directory.
    if (empty($dest) || file_check_path($dest) === FALSE) {
      $dest = file_directory_temp();
    }

    $file->source = $filepath_tmp;
    $file->destination = file_destination(file_create_path($dest .'/'. $file->filename), $replace);
    $file->filesize = filesize($filepath_tmp);

    // Call the validation functions.
    $errors = array();
    foreach ($validators as $function => $args) {
      array_unshift($args, $file);
      // Make sure $file is passed around by reference.
      $args[0] = &$file;
      $errors = array_merge($errors, call_user_func_array($function, $args));
    }

    // Rename potentially executable files, to help prevent exploits.
    $pattern = '/\.(php|pl|py|cgi|asp|js)$/i';
    if (preg_match($pattern, $file->filename) && (drupal_substr($file->filename, -4) != '.txt')) {
      $file->filemime = 'text/plain';
      $file->filepath .= '.txt';
      $file->filename .= '.txt';
      // As the file may be named example.php.txt, we need to munge again to
      // convert to example.php_.txt, then create the correct destination.
      $file->filename = file_munge_filename($file->filename, $extensions);
      $file->destination = file_destination(file_create_path($dest .'/'. $file->filename), $replace);
    }


    // Check for validation errors.
    if (!empty($errors)) {
      // Error
      watchdog('multiuploadalone', 'The selected file @filename could not be uploaded (similar errors suppressed).', array('@filename' => $file->filename), WATCHDOG_NOTICE, NULL);
      // Upload failed: skip to next file
      continue;
    }

    // Move uploaded files from PHP's upload_tmp_dir to Drupal's temporary directory.
    // This overcomes open_basedir restrictions for future file operations.
    $file->filepath = $file->destination;

    // Cannot use move_uploaded_file(), using file_move() instead
    // @see http://api.drupal.org/api/function/file_move/6
    if (!file_move($filepath_tmp, $file->filepath)) {
      // form_set_error($source, t('File upload error. Could not move uploaded file.'));
      watchdog('multiuploadalone', 'Upload error. Could not move uploaded file %file to destination %destination.', array('%file' => $file->filename, '%destination' => $file->filepath));
      // Upload failed: skip to next file
      continue;
    }

    // If we made it this far it's safe to record this file in the database.
    $file->uid = $user->uid;
    $file->status = FILE_STATUS_TEMPORARY;
    $file->timestamp = time();

    // Get default list setting
    $file->list = $list;

    // Get autoincrement fields (other passed by $file)
    drupal_write_record('files', $file);

    $vid = is_null($node->vid) ? $node->nid : $node->vid;

    // Write file record on {upload} and set file status to permanent
    // @see http://api.drupal.org/api/function/upload_save/6
    db_query("INSERT INTO {upload} (fid, nid, vid, list, description, weight) VALUES (%d, %d, %d, %d, '%s', %d)", $file->fid, $node->nid, $vid, $file->list, $file->description, $file->weight);
    // File must be preserved
    file_set_status($file, FILE_STATUS_PERMANENT);
  }
}

/**
 * Delete all files within specified directory, then remove the directory.
 *
 * @param $file
 *   File object. Archive file to remove.
 *
 * @param $dir
 *   Directory to obliterate.
 *
 * @return
 *   An array with status and error information.
 */
function _multiuploadalone_delete($file, $dir) {
  $status = array();
  // delete file and add a line to report for each file (no recursion)
  $files = scandir($dir);
  $ignore = array('.', '..');
  $files = array_diff($files, $ignore);
  if (count($files)) {
    $status['fail_on_upload'] = array();
    foreach ($files as $item) {
      $status['fail_on_upload'][] = $item;
      unlink($dir .'/'. $item);
    }
    $status['errors'] = TRUE;
  }
  else {
    $status['errors'] = FALSE;
  }
  // Directory should now be empty: delete directory
  rmdir($dir);
  // Delete archive file
  file_delete($file->filepath);
  return $status;
}

