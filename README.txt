
Multiple Upload Alone allow to upload a single archive file and save 
the contained files into Drupal as node attachments.

Installation
------------

Copy multiupload.module to your module directory and then enable on the admin
modules page. "unzip" command must be available. Since unzip is launched via 
exec(), this PHP function has to be enabled and available.

PHP5 is required.

Author
------
chirale
